package com.example.listdetail;

import java.util.ArrayList;
import java.util.Map;

public class AnyEventType {
    private String mMsg;
    private ArrayList<Map<String, Object>> list;
    public AnyEventType(String msg,ArrayList<Map<String, Object>> arrayList) {
        // TODO Auto-generated constructor stub
        mMsg = msg;
        list=arrayList;
    }
    public String getMsg(){
        return mMsg;
    }

    public ArrayList<Map<String, Object>> getList() {
        return list;
    }

    public void setList(ArrayList<Map<String, Object>> list) {
        this.list = list;
    }
}
