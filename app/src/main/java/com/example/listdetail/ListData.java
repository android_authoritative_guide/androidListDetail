package com.example.listdetail;

import org.json.JSONObject;

public class ListData {
    private  String id;
    private  String author_id;
    private  String tab;
    private  String title;
    private  String content;
    private  String last_reply_at;
    private  double reply_count;
    private  double visit_count;
    private  String create_at;

   static ListData Instance(JSONObject object){
       ListData t=new ListData();
       try {
           t.setId(object.getString("id"));
           t.setAuthor_id(object.getString("author_id"));
           t.setTab(object.getString("tab"));
           t.setTitle(object.getString("title"));
           t.setLast_reply_at(object.getString("last_reply_at"));
           t.setReply_count(object.getDouble("reply_count"));
           t.setVisit_count(object.getDouble("visit_count"));
           t.setCreate_at(object.getString("last_reply_at"));
           t.setContent(object.getString("content"));
       }catch (Exception e){
           e.printStackTrace();
       }
       return t;
   }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLast_reply_at() {
        return last_reply_at;
    }

    public void setLast_reply_at(String last_reply_at) {
        this.last_reply_at = last_reply_at;
    }

    public double getReply_count() {
        return reply_count;
    }

    public void setReply_count(double reply_count) {
        this.reply_count = reply_count;
    }

    public double getVisit_count() {
        return visit_count;
    }

    public void setVisit_count(double visit_count) {
        this.visit_count = visit_count;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
