package com.example.listdetail;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class MyAdapter extends BaseAdapter {

    private List<ListData> mData;
    private Context mContext;
    private  TextView txt_item_create;
    private  TextView list_visit_count;

    public MyAdapter(List<ListData> mData, Context mContext) {
        this.mData = mData;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_item,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.txt_item_title = (TextView) convertView.findViewById(R.id.txt_item_title);
            txt_item_create= (TextView) convertView.findViewById(R.id.list_create_at);
            list_visit_count= (TextView) convertView.findViewById(R.id.list_visit_count);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Log.d("len3", String.valueOf(mData.size()));
        viewHolder.txt_item_title.setText(mData.get(position).getTitle());
        try{
            String date=StringToDate.StringToDate(mData.get(position).getCreate_at());
            txt_item_create.setText(date);
        }catch (ParseException e){
            e.printStackTrace();
        }
        String dataVisi=String.valueOf(Math.round(mData.get(position).getVisit_count()));
        String visitXml=String.format(mContext.getString(R.string.list_visit),dataVisi);
        list_visit_count.setText(visitXml);
        return convertView;
    }

    private class ViewHolder{
        TextView txt_item_title;
    }

}