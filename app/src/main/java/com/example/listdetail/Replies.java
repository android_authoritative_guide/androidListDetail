package com.example.listdetail;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Replies  {
    private Map<String, Object> arrayList;
    static Map<String, Object> Instance(JSONObject object){
        Replies replies=new Replies();
         Map<String, Object> item = new HashMap<String, Object>();
        try {
            JSONObject author= object.getJSONObject("author");
            item.put("id", object.getString("id"));
            item.put("create_at", object.getString("create_at"));
            item.put("loginname", author.getString("loginname"));
            item.put("avatar_url", author.getString("avatar_url"));
            Log.d("avatar_url",author.getString("avatar_url"));

            Spanned htmlSpan = Html.fromHtml(object.getString("content"));
            item.put("content", htmlSpan);
           replies.setArrayList(item);
        }catch (Exception e){
            e.printStackTrace();
        }
        return replies.arrayList;
    }

    public Map<String, Object> getArrayList() {
        return arrayList;
    }

    public void setArrayList(Map<String, Object> arrayList) {
        this.arrayList = arrayList;
    }
}
