package com.example.listdetail;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;


import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;


public class RepliesFragment extends Fragment {
    private  WebView articleWebView;
    private String picName = "networkPic.jpg";
    private  View view;
    private   Fragment fragment;
    private  FragmentManager fm;
    private ArrayList<Map<String, Object>> datas=new ArrayList<>();
    private ListView list_news;
    private  View readMore;

    RepliesFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //注册EventBus
        EventBus.getDefault().register(this);
//        fm=getActivity().getSupportFragmentManager();
//         fragment=fm.findFragmentById(R.id.fragment_container);
//        if(fragment==null){
//            fragment=CommentFragment.newInstance(2);
//            fm.beginTransaction()
//                    .add(R.id.fragment_container,fragment)
//                    .commit();
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.replies_list, container, false);
        readMore=inflater.inflate(R.layout.replies_move,container,false);
        return view;
    }
    @Subscribe
    public void onEventMainThread(AnyEventType event) {
        String msg = "onEventMainThread收到了消息：" + event.getMsg();
        datas=event.getList();


        Log.d("datasLen", datas.toString());
        List<Map<String, Object>> subDatas= datas.subList(0,4);


        SimpleAdapter myAdapter = new SimpleAdapter(getActivity().getApplicationContext(),
                subDatas,
                R.layout.replies_list_item,
                new String[]{"loginname", "avatar_url", "create_at","content"},
                new int[]{R.id.username, R.id.userImg, R.id.last_reply_at,R.id.content});
        ListView listView = (ListView) view.findViewById(R.id.replies_list);
        listView.setAdapter(myAdapter);
        if(datas.size()>5){
            listView.addFooterView(readMore);
        }
        Log.d("Res111",msg);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);//反注册EventBus
        //fm.beginTransaction().remove(fragment).commit();
    }

}