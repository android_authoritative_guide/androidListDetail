package com.example.listdetail;

import android.content.Context;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * 不同状态的显示站位图(包括无数据，加载异常，正在加载，数据正常以及全部不显示5种状态)
 */
public class StatusSwitchLayout extends RelativeLayout {
    private View vContentView;
    private LinearLayout vRequestLayout;
    private LinearLayout vFailureLayout;
    private LinearLayout vNoDataLayout;

    private ImageView vRequestImg;
    private ImageView vFailureImg;
    private ImageView vNoDataImg;
    private Button vNoDataBtn;

    public StatusSwitchLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initWithContext(context);
    }

    public StatusSwitchLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initWithContext(context);
    }

    public StatusSwitchLayout(Context context) {
        super(context);
        initWithContext(context);
    }

    private void initWithContext(Context context){
        LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.status_switch_layout, this);

        vRequestLayout = (LinearLayout)findViewById(R.id.request_layout);
        vFailureLayout = (LinearLayout)findViewById(R.id.loading_failure_layout);
        vNoDataLayout = (LinearLayout)findViewById(R.id.no_data_layout);

        vRequestImg = (ImageView)findViewById(R.id.request_loading_img);
        vFailureImg = (ImageView)findViewById(R.id.loading_failure_img);
        vNoDataImg = (ImageView)findViewById(R.id.no_data_img);
        vNoDataBtn = (Button)findViewById(R.id.other_operate_button);

    }

    public View getContentView() {
        return vContentView;
    }

    /**
     * 设置content layout 便于统一控制显示哪个layout
     * @param vContentView
     */
    public void setContentView(View vContentView) {
        this.vContentView = vContentView;
    }

    public LinearLayout getRequestLayout() {
        return vRequestLayout;
    }

    public LinearLayout getFailureLayout() {
        return vFailureLayout;
    }

    public LinearLayout getNoDataLayout() {
        return vNoDataLayout;
    }

    public ImageView getRequestImg() {
        return vRequestImg;
    }

    public ImageView getFailureImg() {
        return vFailureImg;
    }

    public ImageView getNoDataImg() {
        return vNoDataImg;
    }

    public Button getNoDataBtn() {
        return vNoDataBtn;
    }

    public void setRequestImgRes(int resId){
        vRequestImg.setImageResource(resId);
    }

    public void setFailureImgRes(int resId){
        vFailureImg.setImageResource(resId);
    }

    public void setNoDataImgRes(int resId){
        vNoDataImg.setImageResource(resId);
    }

    public void setNoDataBtnBg(int resId){
        vNoDataBtn.setBackgroundResource(resId);
    }

    public void showContentLayout(){
        showWhichLayout(0);
    }

    public void showNoDataLayout(){
        showWhichLayout(1);
    }

    public void showRequestLayout(){
        showWhichLayout(2);
    }

    public void showFailureLayout(){
        showWhichLayout(3);
    }

    public void dismissAll(){
        showWhichLayout(4);
    }

    /**
     * 0.代表显示content layout,1.代表显示无数据layout,2.代表显示请求layout,3.代表显示失败layout, 4.代表均不显示(预留显示其他可能的布�?)
     * @param index
     */
    private void showWhichLayout(int index){
        switch (index) {
            case 0:
                if(null != vContentView && vContentView.getVisibility() == View.GONE){
                    showView(vContentView);
                }
                if(vNoDataLayout.getVisibility() == View.VISIBLE){
                    dismissView(vNoDataLayout);
                }
                if(vRequestLayout.getVisibility() == View.VISIBLE){
                    dismissView(vRequestLayout);
                }
                if(vFailureLayout.getVisibility() == View.VISIBLE){
                    dismissView(vFailureLayout);
                }
                break;
            case 1:
                if(null != vContentView && vContentView.getVisibility() == View.VISIBLE){
                    dismissView(vContentView);
                }
                if(vNoDataLayout.getVisibility() == View.GONE){
                    showView(vNoDataLayout);
                }
                if(vRequestLayout.getVisibility() == View.VISIBLE){
                    dismissView(vRequestLayout);
                }
                if(vFailureLayout.getVisibility() == View.VISIBLE){
                    dismissView(vFailureLayout);
                }
                break;
            case 2:
                if(null != vContentView && vContentView.getVisibility() == View.VISIBLE){
                    dismissView(vContentView);
                }
                if(vNoDataLayout.getVisibility() == View.VISIBLE){
                    dismissView(vNoDataLayout);
                }
                if(vRequestLayout.getVisibility() == View.GONE){
                    showView(vRequestLayout);
                }
                if(vFailureLayout.getVisibility() == View.VISIBLE){
                    dismissView(vFailureLayout);
                }
                break;
            case 3:
                if(null != vContentView && vContentView.getVisibility() == View.VISIBLE){
                    dismissView(vContentView);
                }
                if(vNoDataLayout.getVisibility() == View.VISIBLE){
                    dismissView(vNoDataLayout);
                }
                if(vRequestLayout.getVisibility() == View.VISIBLE){
                    dismissView(vRequestLayout);
                }
                if(vFailureLayout.getVisibility() == View.GONE){
                    showView(vFailureLayout);
                }
                break;
            case 4:
                if(null != vContentView && vContentView.getVisibility() == View.VISIBLE){
                    dismissView(vContentView);
                }
                if(vNoDataLayout.getVisibility() == View.VISIBLE){
                    dismissView(vNoDataLayout);
                }
                if(vRequestLayout.getVisibility() == View.VISIBLE){
                    dismissView(vRequestLayout);
                }
                if(vFailureLayout.getVisibility() == View.VISIBLE){
                    dismissView(vFailureLayout);
                }
                break;
            default:
                break;
        }
    }

    private void showView(View view){
        view.setAlpha(0f);
        view.setVisibility(View.VISIBLE);

        view.animate()
                .alpha(1f)
                .setDuration(300)
                .setListener(null);

    }

    private void dismissView(final View view){
        view.setVisibility(View.GONE);
//		view.animate()
//        .alpha(0f)
//        .setDuration(300)
//        .setListener(new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//
//            }
//        });


    }

}